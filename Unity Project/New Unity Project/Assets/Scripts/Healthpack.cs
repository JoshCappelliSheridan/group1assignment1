﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healthpack : MonoBehaviour
{

    public GameObject player;
    public PlayerHealth playerHealth;
    public int HealthBonus = 10;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerHealth = other.gameObject.GetComponent<PlayerHealth>();
            if (playerHealth.currentHealth < playerHealth.startingHealth)
                playerHealth.currentHealth += HealthBonus;
            Destroy(this.gameObject);
        }
    }
}
