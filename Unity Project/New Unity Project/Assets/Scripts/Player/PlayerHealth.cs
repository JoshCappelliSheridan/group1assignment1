﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 250;
    public int currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 4f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);


    Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    //PlayerShooting playerShooting;
    bool isDead;
    bool damaged;


    void Awake ()
    {
        anim = GetComponent <Animator> ();
        if (!anim)
            Debug.LogError("No Animator");

        playerAudio = GetComponent <AudioSource> ();
        if (!playerAudio)
            Debug.LogError("No Player Audio");

        playerMovement = GetComponent <PlayerMovement> ();
        if (!playerMovement)
            Debug.LogError("No Player Movement");

        currentHealth = startingHealth;
    }


    void Update ()
    {
        if(damaged)
        {
            SetDamageImageColor(flashColour);
        }
        else
        {
            SetDamageImageColor(Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime));
        }
        damaged = false;
    }

    public void SetDamageImageColor(Color newColor)
    {
        damageImage.color = newColor;
    }


    public void TakeDamage (int amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        playerAudio.Play ();

        if(currentHealth <= 0 && !isDead)
        {
            Death ();
        }
    }


    void Death ()
    {
        isDead = true;

        //playerShooting.DisableEffects ();

        anim.SetTrigger ("Die");

        playerAudio.clip = deathClip;
        playerAudio.Play ();

        playerMovement.enabled = false;
        //playerShooting.enabled = false;
    }


    public void RestartLevel ()
    {
        SceneManager.LoadScene (0);
    }
}
